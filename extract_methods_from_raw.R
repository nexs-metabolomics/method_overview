library(dplyr)
library(purrr)
library(tidyr)
library(forcats)
library(writexl)

source("FUNS.R")



# Read list of files ------------------------------------------------------
# files <- readLines("I:/SCIENCE-NEXS-NyMetabolomics/Projects/raw_filelist.txt") %>%
#             gsub("\"", "",.) %>%
#             gsub("\\\\", "/",.) %>%
#             trimws() %>%
#             paste0("I:/SCIENCE-NEXS-NyMetabolomics/Projects/",.)


files <- readLines("I:/SCIENCE-NEXS-NyMetabolomics/Projects/list_raw_files.txt", encoding="UTF-8") %>%
            gsub(".*\\./(.*)", "\\1",.) %>%
            paste0("I:/SCIENCE-NEXS-NyMetabolomics/Projects/",.)

# files %>% grep("samples Premeal 1",., value = TRUE) %>% head

# Extract info from files -------------------------------------------------
method_data <- tibble(file = files) %>% 
                mutate(method_data = map(file, get_method_from_raw_p)) %>% 
                unnest(method_data)


# method_data <- method_data %>% mutate(org_input = files)

# missing files
files[!(files %in% method_data$file)] %>% head


# Parse date and time -----------------------------------------------------
Sys.setlocale("LC_ALL","English")

method_data <- method_data %>% 
                mutate(time = as.POSIXct(strptime(paste0(date, " ", time), format = "%d-%b-%Y %H:%M:%S", tz="CET"))) %>% 
                mutate(date = as.Date(time)) %>% 
                mutate(instrument = gsub("^$", "Premier", instrument)) %>%
                mutate(instrument = gsub("HAB298", "Premier", instrument)) %>%
                mutate(instrument = ifelse(grepl("SYNAPT", instrument),"Synapt", instrument)) %>%
                mutate(instrument = ifelse(grepl("TQD", instrument),"TQD", instrument)) %>% 
                mutate(method = factor(method, levels = unique(method)))



# Summarise ---------------------------------------------------------------
method_data_sum <- method_data %>% 
                      group_by(method, instrument) %>% 
                      summarise(n=n(), first_time = min(time), last_time = max(time)) %>% 
                      arrange(desc(n)) %>% 
                      ungroup()



# List example files for each method --------------------------------------
method_data_sum <- method_data_sum %>% 
                      filter(!is.na(method)) %>%
                      filter(!grepl("\\$\\$ Inlet Method:",method)) %>% # method is empty --> calibration
                      mutate(example = map2_chr(method, instrument,  ~ method_data$file[method_data$method %in% ..1 & method_data$instrument %in% ..2][1] ))


method_data_sum %>%
  as.matrix %>% 
  write.csv2("list_methods.csv")



# Get gradient ------------------------------------------------------------
extract_gradient(method_data_sum$example[1])



# Get solvents ------------------------------------------------------------



# Plot time use -----------------------------------------------------------
library(ggplot2)
library(scales)

method_data_sum_filt_sort <- method_data_sum %>% 
                              filter(n > 300) %>% 
                              arrange(desc(first_time)) %>% 
                              mutate(method = factor(method, levels = unique(method)))


p_method_overview <- method_data_sum_filt_sort %>% 
    ggplot(data = ., aes(x = first_time, xend = last_time, y =method, yend = method)) +
    geom_segment(size = 3) +
    theme_bw() +
    labs(x = "Time") +
    facet_grid(instrument~., scales = "free_y", space = "free_y") +
    scale_x_datetime(breaks  = as.POSIXct(seq.Date(as.Date("1900-01-01"), as.Date("3000-01-01"), by ="year")), labels = date_format("%Y"))




    



p_method_overview_points <- 
  p_method_overview +
  geom_point(data=filter(method_data, instrument %in% method_data_sum_filt_sort$instrument & method %in% method_data_sum_filt_sort$method), 
             aes(time, method), 
             color = "red", 
             size = 0.1, 
             inherit.aes = FALSE
             )

p_method_overview_points

ggsave("p_method_overview_points.png", p_method_overview_points, width = 12, height = 6, dpi = 300)


write_xlsx(method_data,"method_data.csv")

